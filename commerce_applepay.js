(function($) {
  /**
   * Disable the continue buttons in the checkout process once they are clicked
   * and provide a notification to the user.
   */
  Drupal.behaviors.commerceApplePay = {
    attach: function (context, settings) {

      // Init stripe once.
      $('body', context).once('commerce-applepay-stripe', function () {
        Stripe.setPublishableKey(Drupal.settings.commerce_applepay_stripe.stripe_key);
      });

      // Convert empty extra settings from array to object for better developer experience.
      if (settings.commerce_applepay_stripe && $.isArray(settings.commerce_applepay_stripe.extra) && settings.commerce_applepay_stripe.extra.length == 0) {
        settings.commerce_applepay_stripe.extra = {};
      }

      $('#apple-pay-button, .commerce-applepay-set-up-button', context).once('commerce-applepay-stripe').click(function (ev) {
        ev.preventDefault();
        Drupal.behaviors.commerceApplePay.beginApplePay();
      });

      // Check if Apple Pay tab active.
      var activePaymentType = $('input[name="commerce_payment[payment_method]"]:checked', context).val();
      if (typeof activePaymentType !== 'undefined') {
        // Don't use context here, because it usually returns form object itself.
        var $form = $('.commerce-checkout-form-checkout');

        // Check if Apple Pay tab active.
        if (activePaymentType.indexOf('commerce_applepay_stripe') >= 0) {
          $form.once('commerce-applepay-stripe', function () {

            // Check Apple Pay availability status.
            Drupal.commerceApplePay.availabilityStatus(function (availabilityStatus) {

              // Apple Pay is available for payment.
              if (availabilityStatus == Drupal.commerceApplePay.APPLE_PAY_AVAILABLE) {

                Drupal.behaviors.commerceApplePay.showButton();
              }
              // Apple Pay is available for payment, but not configured.
              else if (availabilityStatus == Drupal.commerceApplePay.APPLE_PAY_NOT_CONFIGURED) {

                $form.addClass('commerce-applepay-not-configured');
                Drupal.behaviors.commerceApplePay.showSetUpButton();
              }
            });
          });
        }
        // Hides buttons and removes class if Apple pay tab isn't active.
        else {
          Drupal.behaviors.commerceApplePay.hideButtons();
          $form.removeClass('commerce-applepay-not-configured');
        }
      }
    },

    showButton: function () {
      $('.checkout-continue').hide();
      $('.apple-pay-setup-wrapper').hide();
      $('#apple-pay-button').show();
    },

    showSetUpButton: function () {
      $('.checkout-continue').hide();
      // Shows wrapper and button, because Stripe hides Setup button by default.
      $('.apple-pay-setup-wrapper, .commerce-applepay-set-up-button').show();
    },

    hideButtons: function () {
      $('#apple-pay-button').hide();
      $('.apple-pay-setup-wrapper').hide();
      $('.checkout-continue').show();
    },
    
    beginApplePay: function () {

      var ev = jQuery.Event('applepaybuildsession');
      $('#apple-pay-button').trigger(ev);

      // Allow other scripts to cancel payment process.
      if (ev.isDefaultPrevented()) {
        return;
      }

      // Will use test donation process only if test mode enabled and there is 'applepay_availability' param in _GET query.
      var applepay_availability = Drupal.commerceApplePay.getUrlVars()['applepay_availability'];
      var is_test_mode_enabled = typeof Drupal.settings.commerce_applepay_stripe.extra.test_mode !== 'undefined' ? Drupal.settings.commerce_applepay_stripe.extra.test_mode : false;
      if (typeof applepay_availability !== 'undefined' && is_test_mode_enabled) {

        var charge_path = Drupal.settings.basePath + Drupal.settings.pathPrefix + 'checkout/' + Drupal.settings.commerce_applepay_stripe.order_id + '/applepay-test';
        var charge_data = {
          payment_method_id: Drupal.settings.commerce_applepay_stripe.payment_method,
          payment_request: Drupal.settings.commerce_applepay_stripe.paymentRequest,
          // TODO: check generic form class for all Drupal Commerce installations.
          serialized_form: $('.commerce-checkout-form-checkout').formSerialize(),
          extra: Drupal.settings.commerce_applepay_stripe.extra
        };
        $.post(charge_path, {data: charge_data}).done(function (data) {
          window.location.href = data.redirect_uri;
        });
        return;
      }

      // Build Apple Pay session.
      var session = Stripe.applePay.buildSession(Drupal.settings.commerce_applepay_stripe.paymentRequest, function (result, completion) {

        //  Set Up button hides Apple Pay button. Cancel payment in this case.
        // TODO: reload page automatically when set up process is complete.
        if ($('#apple-pay-button').is(':hidden')) {
          window.location.reload();
        }

        // Prepare data for backend call.
        var charge_data = {
          token: result.token.id,
          result: result,
          payment_method_id: Drupal.settings.commerce_applepay_stripe.payment_method,
          payment_request: Drupal.settings.commerce_applepay_stripe.paymentRequest,
          // TODO: check generic form class for all Drupal Commerce installations.
          serialized_form: $('.commerce-checkout-form-checkout').formSerialize(),
          extra: Drupal.settings.commerce_applepay_stripe.extra
        };

        var charge_path = Drupal.settings.basePath + Drupal.settings.pathPrefix + Drupal.settings.commerce_applepay_stripe.charge_path;

        // Make backend call to charge money.
        $.post(charge_path, {data: charge_data}).done(function (data) {
          completion(ApplePaySession.STATUS_SUCCESS);

          var ev = jQuery.Event('applepaysuccess');
          ev.charge_data = charge_data;
          ev.server_response = data;
          $('#apple-pay-button').trigger(ev);

          // Redirect to complete page.
          window.location.href = data.redirect_uri;

        }).fail(function (data) {
          completion(ApplePaySession.STATUS_FAILURE);
          var ev = jQuery.Event('applepayerror');
          ev.error_message = data.responseText;
          $('#apple-pay-button').trigger(ev);
        });

      }, function (error) {
        var ev = jQuery.Event('applepayerror');
        ev.error_message = error.message;
        $('#apple-pay-button').trigger(ev);
      });

      session.begin();
    }
  };

  Drupal.commerceApplePay = {};
  Drupal.commerceApplePay.APPLE_PAY_AVAILABLE = 'available';
  Drupal.commerceApplePay.APPLE_PAY_NOT_CONFIGURED = 'not_configured';
  Drupal.commerceApplePay.APPLE_PAY_NOT_AVAILABLE = 'not_available';

  /**
   * Checks status of Apple Pay configuration.
   * Passes availabilityStatus variable to callback function.
   * availabilityStatus can be 'available', 'not_configured' or 'not_available'.
   *
   * @param callback
   *   Function name, thank will be executed.
   */
  Drupal.commerceApplePay.availabilityStatus = function(callback) {
    // Set default value for variable.
    var availabilityStatus = Drupal.commerceApplePay.APPLE_PAY_NOT_AVAILABLE;
    var is_test_mode_enabled = typeof Drupal.settings.commerce_applepay_stripe.extra.test_mode !== 'undefined' ? Drupal.settings.commerce_applepay_stripe.extra.test_mode : false;

    // Check applepay_availability parameter in GET query, it's used for testing purposes.
    var applepay_availability = Drupal.commerceApplePay.getUrlVars()['applepay_availability'];
    if (typeof applepay_availability !== 'undefined' && is_test_mode_enabled) {

      if (applepay_availability == Drupal.commerceApplePay.APPLE_PAY_AVAILABLE) {
        availabilityStatus = Drupal.commerceApplePay.APPLE_PAY_AVAILABLE;
      }
      else if (applepay_availability == Drupal.commerceApplePay.APPLE_PAY_NOT_CONFIGURED) {
        availabilityStatus = Drupal.commerceApplePay.APPLE_PAY_NOT_CONFIGURED;
      }
      callback(availabilityStatus);
      return;
    }

    // If it's real device.
    if (window.ApplePaySession && ApplePaySession.canMakePayments) {

      Stripe.applePay.checkAvailability(function(available) {

        availabilityStatus = available ? Drupal.commerceApplePay.APPLE_PAY_AVAILABLE : Drupal.commerceApplePay.APPLE_PAY_NOT_CONFIGURED;
        callback(availabilityStatus);
      });
      return;
    }
    callback(availabilityStatus);
  };

  /**
   * Helper function to return array of _GET parameters.
   *
   * @returns {Array}
   *   An array of Get parameters.
   */
  Drupal.commerceApplePay.getUrlVars = function() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  };


})(jQuery);
