<?php

/**
 * @file
 * Drupal API documentation for Commerce Apple Pay module.
 */

/**
 * Allows modules to alter JS settings for Apple Pay clientside configuration.
 *
 * @param $js_settings_data
 *   This data will be available on the fronend as
 *   Drupal.settings.commerce_applepay_stripe.
 *   Array with the following keys:
 *   - payment_method: payment method instance id
 *   - charge_path: path on the server to complete Stripe charge
 *   - paymentRequest: Apple Pay payment request.
 *     See https://developer.apple.com/reference/applepayjs/paymentrequest
 *
 * @param $order
 *   Commerce order object.
 * @param $context
 *   An array of contextual information including keys:
 *   - payment_method
 *   - pane_values
 *   - checkout_pane
 *
 */
function hook_commerce_applepay_payment_request_alter(&$js_settings_data, $order, $context) {
  // Require billing address from Apple Pay.
  // Can be used with _commerce_applepay_customer_profile_address().
  $js_settings_data['paymentRequest']['requiredBillingContactFields'] = ['postalAddress'];
  // Require email address from Apple Pay.
  $js_settings_data['paymentRequest']['requiredShippingContactFields'] = ['email'];
}

/**
 * Allows other modules to alter response data.
 *
 * @param $response
 *   An array contains response data from Stripe.
 * @param $order
 *   Commerce order object.
 */
function hook_commerce_applepay_payment_response_alter(&$response, $order) {
  $complete_url = custom_module_commerce_checkout_complete_redirect_url($order);
  if (!empty($complete_url)) {
    $response['redirect_uri'] = $complete_url;
  }
}

/**
 * Allow other modules to update context and order before the transaction is completed.
 *
 * Apple Pay doesn't submit the checkout form. Instead, all form data is sent
 * via AJAX and is passed in alter hook in $context['form_values'].
 *
 * @param $order
 *   Commerce order object.
 * @param $context
 *   An array of contextual information including:
 *   - payment_request: original paymentRequest array sent to Apple Pay API
 *   - payment_method: payment method array
 *   - stripe_data: result from Stripe fronend callback with all customer data
 *   - form_values: all checkout form values passed to the backend via AJAX
 *   - metadata: any metadata to be saved in Stripe
 */
function hook_commerce_applepay_stripe_context_alter(&$order, &$context) {
  // Create customer profile and save billing address.
  _commerce_applepay_customer_profile_address($order, $context['stripe_data']);
}
