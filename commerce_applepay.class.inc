<?php

/**
 * @file
 * Contains the CommerceApplePayInterface interface.
 */

/**
 * Interface representing Commerce Apple Pay backend class.
 */
interface CommerceApplePayInterface {

  /**
   * CommerceApplePayInterface constructor.
   * @param $order
   *   Commerce order.
   */
  public function __construct($order);

  /**
   * Make payment transaction.
   *
   * @throws Exception
   */
  public function makeTransaction();

  /**
   * Complete the order after successful transaction.
   *
   * @return array
   *   Data for JSON output to the frontend.
   */
  public function finalizeOrder();

}

/**
 * Implementation of one-time Apple Pay payment (through Stripe).
 */
class CommerceApplePayStripe implements CommerceApplePayInterface {

  protected $order;
  protected $context = array();

  /**
   * CommerceApplePayStripe constructor.
   * @param $order
   *   Commerce order.
   */
  public function __construct($order) {
    $this->order = $order;
    $this->context = $this->initGlobalContext();
  }

  /**
   * Helper to get payment setting from context array.
   *
   * @param string $setting_name
   *   Setting name.
   *
   * @return mixed
   *   Requested setting.
   */
  public function getPaymentSetting($setting_name) {
    return $this->context['payment_method']['settings'][$setting_name];
  }

  /**
   * Get all required data from global scope.
   *
   * @throws Exception
   */
  protected function initGlobalContext() {
    // Include Stripe library.
    $library = libraries_get_path('stripe-php');
    require_once($library . '/init.php');

    $context = array(
      'payment_request' => array(),
      'payment_method' => array(),
      'stripe_data' => array(),
      'form_values' => array(),
      'metadata' => array(),
      'extra' => array(),
    );

    // Form values are passed as a serialized string.
    $form_values = array();
    parse_str($_POST['data']['serialized_form'], $form_values);

    if (empty($_POST['data']['token']) || empty($_POST['data']['payment_method_id']) || empty($_POST['data']['payment_request']) || empty($_POST['data']['result']) || empty($form_values)) {
      // Server will return 500 to the frontend.
      throw new Exception('Cannot initialize CommerceApplePayStripe class with empty or incorrect data.');
    }

    // Get stripe backend key and init it.
    $payment_method = commerce_payment_method_instance_load($_POST['data']['payment_method_id']);

    $context['payment_request'] = $_POST['data']['payment_request'];
    $context['payment_method'] = $payment_method;
    $context['stripe_data'] = $_POST['data']['result'];
    $context['form_values'] = $form_values;
    $context['metadata'] = array("order_id" => $this->order->order_id);
    $context['extra'] = isset($_POST['data']['extra']) ? $_POST['data']['extra'] : array();

    // Allow other modules to alter data / update order before the transaction.
    drupal_alter('commerce_applepay_stripe_context', $this->order, $context);

    // Reload the order after possible changes in alter hooks.
    $this->order = commerce_order_load($this->order->order_id);

    // Init Stripe.
    $stripe_secret_key = $payment_method['settings']['stripe_secret_key'];
    \Stripe\Stripe::setApiKey($stripe_secret_key);

    return $context;
  }

  /**
   * Make single transaction.
   *
   * @throws \Stripe\Error\Base
   */
  public function makeTransaction() {
    $order = $this->order;
    $context = $this->context;

    $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    $order_total_data = $order_wrapper->commerce_order_total->value();
    $payment_method_id = $context['payment_method']['instance_id'];

    try {
      // Charge the user's card immediately.
      $charge = \Stripe\Charge::create(array(
        // NOTE: Stripe uses the same amount encoding as Drupal
        // Commerce (1050 = 10.50).
        'amount' => $order_total_data['amount'],
        'currency' => $order_total_data['currency_code'],
        'description' => $context['payment_request']['total']['label'],
        'metadata' => $context['metadata'],
        'source' => $context['stripe_data']['token']['id'],
      ));

      // Prepare a transaction object to log the API response.
      $transaction = commerce_payment_transaction_new('commerce_applepay_stripe', $order->order_id);
      $transaction->instance_id = $payment_method_id;
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];
      $transaction->payload[REQUEST_TIME] = $charge->getLastResponse()->json;
      $transaction->remote_id = $charge->id;
      $transaction->remote_status = $charge->status;
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = 'Stripe charge created successfully.';
      commerce_payment_transaction_save($transaction);
    }
    catch (\Stripe\Error\Base $e) {

      $transaction = commerce_payment_transaction_new('commerce_applepay_stripe', $order->order_id);
      $transaction->instance_id = $payment_method_id;
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];
      $transaction->payload[REQUEST_TIME] = $e->getJsonBody();
      $transaction->remote_status = 'error';
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = 'Could not complete Stripe charge. ' . $e->getMessage();
      commerce_payment_transaction_save($transaction);

      watchdog_exception('commerce_applepay', $e);

      // Server will return 500 to the frontend.
      throw $e;
    }
  }

  /**
   * Complete the order after successful transaction.
   */
  public function finalizeOrder() {
    commerce_order_status_update($this->order, 'checkout_complete', FALSE, FALSE);
    commerce_checkout_complete($this->order);

    $response = array(
      'redirect_uri' => url(commerce_checkout_order_uri($this->order)),
    );

    // Allows other modules to alter response data.
    drupal_alter('commerce_applepay_payment_response', $response, $this->order);

    return $response;
  }

}

/**
 * Implementation of recurring Apple Pay payment (through Stripe).
 */
class CommerceApplePayStripeRecurring extends CommerceApplePayStripe {

  /**
   * Amount for Stripe recurring plan. 1 (USD) = 0.01 (USD).
   */
  const RECURRING_PLAN_AMOUNT = 1;

  /**
   * Make recurring transaction.
   *
   * @throws \Stripe\Error\Base
   */
  public function makeTransaction() {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    $order_total_data = $order_wrapper->commerce_order_total->value();
    $payment_method_id = $this->context['payment_method']['instance_id'];

    try {
      // Get plan for current payment method.
      $plan_id = $this->getPlanId();
      $plan = $this->getPlan($plan_id);
      $customer = $this->getCustomer($order_total_data['currency_code']);

      // Subscribe the customer to the plan.
      $subscription = \Stripe\Subscription::create(array(
        'customer' => $customer->id,
        'plan' => $plan_id,
        'quantity' => $this->getSubscriptionQuantity($order_total_data['amount']),
        'metadata' => $this->context['metadata'] + array('amount' => commerce_currency_format($order_total_data['amount'], $order_total_data['currency_code'])),
      ));

      // Prepare a transaction object to log the API response.
      $transaction = commerce_payment_transaction_new('commerce_applepay_stripe', $this->order->order_id);
      $transaction->instance_id = $payment_method_id;
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];
      $transaction->payload[REQUEST_TIME] = $subscription->getLastResponse()->json;
      $transaction->remote_id = $subscription->id;
      $transaction->remote_status = $subscription->status;
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = 'Stripe subscription created successfully.';
      commerce_payment_transaction_save($transaction);

    }
    catch (\Stripe\Error\Base $e) {

      $transaction = commerce_payment_transaction_new('commerce_applepay_stripe', $this->order->order_id);
      $transaction->instance_id = $payment_method_id;
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];
      $transaction->payload[REQUEST_TIME] = $e->getJsonBody();
      $transaction->remote_status = 'error';
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = 'Could not subscribe customer to a plan. ' . $e->getMessage();
      commerce_payment_transaction_save($transaction);
      watchdog_exception('commerce_applepay', $e);

      // Server will return 500 to the frontend.
      throw $e;
    }
  }

  /**
   * Build plan id string based on payment method params.
   *
   * @return string
   *   Plan ID in format:
   *   commerce_applepay_{INTERVAL}_{INTERVAL_COUNT}_{CURRENCY}
   *   Example: commerce_applepay_monthly_1_gbp.
   */
  public function getPlanId() {
    $plan_id = 'commerce_applepay_' . $this->getPaymentSetting('recurring_billing_interval') . '_' . $this->getPaymentSetting('recurring_billing_interval_count') . '_' . $this->getPaymentSetting('currency');

    return strtolower($plan_id);
  }

  /**
   * Retrieves or creates plan by id based on payment method settings.
   *
   * @return \Stripe\Plan
   *   Stripe plan.
   */
  protected function getPlan($plan_id) {

    try {
      $plan = \Stripe\Plan::retrieve($plan_id);
    }
    catch (\Stripe\Error\Base $e) {

      // Create plan if it doesn't exist.
      $plan = \Stripe\Plan::create(array(
        'amount' => self::RECURRING_PLAN_AMOUNT,
        'interval' => $this->getPaymentSetting('recurring_billing_interval'),
        'interval_count' => $this->getPaymentSetting('recurring_billing_interval_count'),
        'trial_period_days' => $this->getPaymentSetting('recurring_billing_trial_period_days'),
        'name' => 'Commerce Apple Pay ' . ucfirst($this->getPaymentSetting('recurring_billing_interval')) . strtoupper($this->getPaymentSetting('currency')),
        'currency' => $this->getPaymentSetting('currency'),
        'id' => $plan_id,
      ));
    }

    return $plan;
  }

  /**
   * Retrieves or creates a customer based on Drupal user from the order.
   *
   * @param string $currency
   *   Customer currency in Stripe. Stripe doesn't support multi-currency users.
   *
   * @return \Stripe\Customer
   *   Stripe customer.
   */
  protected function getCustomer($currency) {

    $currency = strtolower($currency);

    // Try to find existing account.
    if (!empty($this->order->uid)) {
      if ($account = user_load($this->order->uid)) {
        if (!empty($account->data["stripe_customer_id_{$currency}"])) {
          try {
            $customer = \Stripe\Customer::retrieve($account->data['stripe_customer_id']);

            // Customer is not deleted and it's currency is valid.
            if (empty($customer->deleted) && $customer->currency == $currency) {
              return $customer;
            }
          }
          catch (\Stripe\Error\Base $e) {
            // Customer not found. It will be created in the code below.
          }
        }
      }
    }

    // Create new customer with Stripe source token.
    $customer = \Stripe\Customer::create(array(
      'email' => $this->order->mail,
      "source" => $this->context['stripe_data']['token']['id'],
      'metadata' => array('commerce_applepay' => 1),
    ));

    // Save Stripe id in user object.
    if (!empty($this->order->uid) && $account = user_load($this->order->uid)) {
      user_save($account, array('data' => array("stripe_customer_id_{$currency}" => $customer->id)));
    }

    return $customer;
  }

  /**
   * Calculate subscription quantity for given amount.
   *
   * See https://support.stripe.com/questions/how-can-i-create-plans-that-dont-have-a-fixed-price
   *
   * @param int $amount
   *   Drupal Commerce amount. 1000 = 10.00.
   *
   * @return int
   *   Quantity for subscription.
   */
  public function getSubscriptionQuantity($amount) {
    return $amount / self::RECURRING_PLAN_AMOUNT;
  }

}

/**
 * Implementation of Test Apple Pay payment (through Stripe).
 */
class CommerceApplePayStripeTestMode extends CommerceApplePayStripe {

  /**
   * CommerceApplePayStripeTestMode constructor.
   * @param $order
   *   Commerce order.
   */
  public function __construct($order) {
    parent::__construct($order);
    if (empty($this->context['payment_method']['settings']['test_mode'])) {
      drupal_not_found();
    }
  }

  /**
   * Get all required data from global scope.
   *
   * @throws Exception
   */
  protected function initGlobalContext() {
    $context = array(
      'payment_request' => array(),
      'payment_method' => array(),
      'stripe_data' => array(),
      'form_values' => array(),
      'metadata' => array(),
      'extra' => array(),
    );

    // Form values are passed as a serialized string.
    $form_values = array();
    parse_str($_POST['data']['serialized_form'], $form_values);

    if (empty($_POST['data']['payment_method_id']) || empty($_POST['data']['payment_request']) || empty($form_values)) {
      // Server will return 500 to the frontend.
      throw new Exception('Cannot initialize CommerceApplePayStripe class with empty or incorrect data.');
    }

    // Get stripe backend key and init it.
    $payment_method = commerce_payment_method_instance_load($_POST['data']['payment_method_id']);

    $context['payment_request'] = $_POST['data']['payment_request'];
    $context['payment_method'] = $payment_method;
    $context['form_values'] = $form_values;
    $context['metadata'] = array("order_id" => $this->order->order_id);
    $context['extra'] = isset($_POST['data']['extra']) ? $_POST['data']['extra'] : array();

    // Fill Test stripe data. @todo: move to custom module?
    $context['stripe_data'] = array(
      'token' => array(
        'id' => 'tok_19kWcEL5VstormvXGX3Vxi2j',
        'object' => 'token',
        'card' => array(
          'id' => 'card_19kWcEL5VstormvXQDr7tiU9',
          'object' => 'card',
          'address_city' => 'London',
          'address_country' => 'gb',
          'address_line1' => 'Street 1',
          'address_line1_check' => 'unchecked',
          'address_line2' => 'Street 2',
          'address_state' => 'Greater London',
          'address_zip' => '100WQ1',
          'address_zip_check' => 'unchecked',
          'brand' => 'Visa',
          'country' => 'US',
          'cvc_check' => 'null',
          'dynamic_last4' => '3763',
          'exp_month' => '11',
          'exp_year' => '2025',
          'funding' => 'prepaid',
          'last4' => '0492',
          'name' => 'Tester',
          'tokenization_method' => 'apple_pay',
          ),
        'client_ip' => '121.108.28.200',
        'created' => '1486464870',
        'livemode' => 'false',
        'type' => 'card',
        'used' => 'false',
      ),
      'shippingContact' => array(
        'emailAddress' => 'test-applepay-payment@test.test',
        'givenName' => '',
        'familyName' => '',
        'phoneNumber' => '+29818474627',
      ),
    );

    // Allow other modules to alter data / update order before the transaction.
    drupal_alter('commerce_applepay_stripe_context', $this->order, $context);

    // Reload the order after possible changes in alter hooks.
    $this->order = commerce_order_load($this->order->order_id);

    return $context;
  }

  /**
   * Make single transaction without request to Stripe.
   */
  public function makeTransaction() {
    $order = $this->order;
    $context = $this->context;

    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_total_data = $order_wrapper->commerce_order_total->value();

    try {
      // Prepare a transaction object to log the API response.
      $transaction = commerce_payment_transaction_new('commerce_applepay_stripe', $order->order_id);
      $transaction->instance_id = $context['payment_method']['instance_id'];
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];

      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = 'Test Stripe charge created successfully.';
      commerce_payment_transaction_save($transaction);
    }
    catch (Exception $e) {

      $transaction = commerce_payment_transaction_new('commerce_applepay_stripe', $order->order_id);
      $transaction->instance_id = $context['payment_method']['instance_id'];
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];
      $transaction->remote_status = 'error';
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = 'Could not complete Test Stripe charge. ' . $e->getMessage();
      commerce_payment_transaction_save($transaction);

      watchdog_exception('commerce_applepay', $e);

      // Server will return 500 to the frontend.
      throw $e;
    }
  }
}
