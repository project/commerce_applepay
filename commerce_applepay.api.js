(function($) {
  Drupal.behaviors.exampleContribModule.attach = function(context, settings) {
    /**
     * 'applepaybuildsession' is fired on before Apple Pay submit.
     */
    $('#apple-pay-button', context).on('applepaybuildsession', function (ev) {
      // Other modules have a chance to cancel payment process.
      ev.preventDefault();

      // Other modules have a chance to alter paymentRequest data.
      Drupal.settings.commerce_applepay_stripe.paymentRequest.total.amount = '10.00';
      // Please note that you will have to update commerce order amount on backend in
      // hook_commerce_applepay_stripe_context_alter().
    });

    /**
     * 'applepaysuccess' is fired after successful Apple Pay payment and before
     * final redirect to checkout complete page.
     */
    $('#apple-pay-button', context).on('applepaysuccess', function (ev) {
      // Data that has been sent to the backend.
      var charge_data = ev.charge_data;
      // Data that has been received from the backend.
      var server_data = ev.server_response;
    });

    /**
     * 'applepayerror' is fired in case of error either from Stripe or from Drupal backend.
     */
    $('#apple-pay-button', context).on('applepayerror', function (ev) {
      console.log(ev.error_message);
    });
  }

})(jQuery);
